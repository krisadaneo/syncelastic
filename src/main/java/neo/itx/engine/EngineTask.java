/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.itx.engine;

import java.util.Hashtable;
import java.util.List;
import neo.itx.engine.service.DBService;
import net.sf.cglib.reflect.FastClass;
import net.sf.cglib.reflect.FastMethod;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;

/**
 *
 * @author krisada
 */
public class EngineTask implements Runnable {

    private Logger log = LoggerFactory.getLogger(this.getClass());
    private String sessionId;
    private int threadNo;
    private DBService dbService;
    private ElasticsearchOperations elasticsearchOperations;
    private String indexCoordinate;
    private SynchronizeQuery sync;
    private Hashtable params;
    private int offset = 0;
    private int end = 0;
    private String host;
    
    public EngineTask(String host, String sessionId, int threadNo, DBService dbService, ElasticsearchOperations elasticsearchOperations,
            String indexCoordinate, SynchronizeQuery sync, Hashtable params, int offset, int end) {
        this.host = host;
        this.sessionId = sessionId;
        this.threadNo = threadNo;
        this.dbService = dbService;
        this.elasticsearchOperations = elasticsearchOperations;
        this.indexCoordinate = indexCoordinate;
        this.sync = sync;
        this.params = params;
        this.offset = offset;
        this.end = end;
    }

    @Override
    public void run() {
        log.info(String.format("Starting Thread No=%d running...", threadNo));
        String fieldUid = (String) sync.getParam("uid");
        try {
            List ls = dbService.find(params, sync.getQuery().toLowerCase(), fieldUid, offset, end);
            log.info(String.format("uid=%s, size=%d", fieldUid, ls.size()));
            Transformer transformer = new Transformer();
            StringBuilder sb = new StringBuilder();
            String prop = transformer.getSetPropertyName(sb, fieldUid);
            String mprop = transformer.getSetMethodName("get", sb, fieldUid);
            Class[] emptyClassArray = new Class[0];
            Object[] emptyObjectArray = new Object[0];
            for (int i = 0; i < ls.size(); i++) {
                Object o = ls.get(i);
                FastClass fc = FastClass.create(o.getClass());
                FastMethod fm = fc.getMethod(mprop, emptyClassArray);
                String uid = String.valueOf(fm.invoke(o, emptyObjectArray));
                log.info(String.format("FieldUid=%s, UID=%s:", fieldUid, uid));
                JSONObject searchHit = ElasticSearchClient.client().search(
                        this.host, indexCoordinate.toLowerCase(), prop, uid);
                if (searchHit != null) {
                    String id = searchHit.getString("_id");
                    double score = searchHit.getDouble("_score");
                    log.info(String.format("searchHit id: %s, score: %,.7f", 
                            id, score));
                    elasticsearchOperations.delete(id, IndexCoordinates.of(indexCoordinate.toLowerCase()));
                    elasticsearchOperations.save(o, IndexCoordinates.of(indexCoordinate.toLowerCase()));
                } else {
                    elasticsearchOperations.save(o, IndexCoordinates.of(indexCoordinate.toLowerCase()));
                }
                Thread.sleep(200);
            }
            int now = dbService.findTransactionStatus(sessionId);
            dbService.saveTransactionStatus(sessionId, now + ls.size());
        } catch (Exception ex) {
            log.error("runnung error:", ex);
            ex.printStackTrace();
        }
        log.info(String.format("Ending Thread No=%d running...", threadNo));
    }

    private String getSetMethodName(String getOrSet, StringBuilder s, String columnName) {
        String col[] = columnName.split("_");
        for (int i = 0; i < col.length; i++) {
            String px = col[i].substring(0, 1).toUpperCase();
            String sx = col[i].substring(1, col[i].length()).toLowerCase();
            s.append(px).append(sx);
        }
        String met = s.toString();
        s.delete(0, s.length());
        return String.format("%s%s", getOrSet, met);
    }
}
