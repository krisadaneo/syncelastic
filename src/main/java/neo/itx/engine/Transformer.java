/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.itx.engine;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import net.sf.cglib.beans.BeanGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author krisada
 */
public class Transformer {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    public String getSetMethodName(String getOrSet, StringBuilder s, String columnName) {
        String col[] = columnName.split("_");
        for (int i = 0; i < col.length; i++) {
            String px = col[i].substring(0, 1).toUpperCase();
            String sx = col[i].substring(1, col[i].length()).toLowerCase();
            s.append(px).append(sx);
        }
        String met = s.toString();
        s.delete(0, s.length());
        return String.format("%s%s", getOrSet, met);
    }

    public String getSetPropertyName(StringBuilder s, String columnName) {
        String col[] = columnName.split("_");
        for (int i = 0; i < col.length; i++) {
            String px = null;
            if (i == 0) {
                px = col[i].substring(0, 1).toLowerCase();
            } else {
                px = col[i].substring(0, 1).toUpperCase();
            }
            String sx = col[i].substring(1, col[i].length()).toLowerCase();
            s.append(px).append(sx);
        }
        String met = s.toString();
        s.delete(0, s.length());
        return met;
    }

    public void invoke(StringBuilder buffer, int type, String name,
            BeanGenerator bean1, Object o, ResultSet rs) throws Exception {
        Method method = null;
        switch (type) {
            case java.sql.Types.CHAR:
            case java.sql.Types.VARCHAR:
            case java.sql.Types.LONGVARCHAR:
                String val1 = rs.getString(name);
                method = o.getClass().getMethod(getSetMethodName("set", buffer, name), new Class[]{String.class});
                method.invoke(o, new String[]{val1});
                break;
            case java.sql.Types.NUMERIC:
            case java.sql.Types.DECIMAL:
                BigDecimal val2 = rs.getBigDecimal(name);
                method = o.getClass().getMethod(getSetMethodName("set", buffer, name), new Class[]{java.math.BigDecimal.class});
                method.invoke(o, new BigDecimal[]{val2});
                break;
            case java.sql.Types.BIT:
                Boolean val3 = rs.getBoolean(name);
                method = o.getClass().getMethod(getSetMethodName("set", buffer, name), new Class[]{java.lang.Boolean.class});
                method.invoke(o, new Boolean[]{val3});
                break;
            case java.sql.Types.TINYINT:
                Byte val4 = rs.getByte(name);
                method = o.getClass().getMethod(getSetMethodName("set", buffer, name), new Class[]{java.lang.Byte.class});
                method.invoke(o, new Byte[]{val4});
                break;
            case java.sql.Types.SMALLINT:
                Short val5 = rs.getShort(name);
                method = o.getClass().getMethod(getSetMethodName("set", buffer, name), new Class[]{java.lang.Short.class});
                method.invoke(o, new Short[]{val5});
                break;
            case java.sql.Types.INTEGER:
                Integer val6 = rs.getInt(name);
                method = o.getClass().getMethod(getSetMethodName("set", buffer, name), new Class[]{java.lang.Integer.class});
                method.invoke(o, new Integer[]{val6});
                break;
            case java.sql.Types.BIGINT:
                Long val7 = rs.getLong(name);
                method = o.getClass().getMethod(getSetMethodName("set", buffer, name), new Class[]{java.lang.Long.class});
                method.invoke(o, new Long[]{val7});
                break;
            case java.sql.Types.REAL:
                Float val8 = rs.getFloat(name);
                method = o.getClass().getMethod(getSetMethodName("set", buffer, name), new Class[]{java.lang.Float.class});
                method.invoke(o, new Float[]{val8});
                break;
            case java.sql.Types.FLOAT:
            case java.sql.Types.DOUBLE:
                Double val9 = rs.getDouble(name);
                method = o.getClass().getMethod(getSetMethodName("set", buffer, name), new Class[]{java.lang.Double.class});
                method.invoke(o, (Object) new Double[]{val9});
                break;
            case java.sql.Types.BINARY:
            case java.sql.Types.VARBINARY:
            case java.sql.Types.LONGVARBINARY:
                byte[] val10 = rs.getBytes(name);
                method = o.getClass().getMethod(getSetMethodName("set", buffer, name), new Class[]{java.lang.Byte[].class});
                method.invoke(o, new byte[][]{val10});
                break;
            case java.sql.Types.DATE:
                java.util.Date val11 = new java.util.Date(rs.getDate(name).getTime());
                method = o.getClass().getMethod(getSetMethodName("set", buffer, name), new Class[]{java.util.Date.class});
                method.invoke(o, new java.util.Date[]{val11});
                break;
            case java.sql.Types.TIME:
                java.sql.Time val12 = rs.getTime(name);
                method = o.getClass().getMethod(getSetMethodName("set", buffer, name), new Class[]{java.sql.Time.class});
                method.invoke(o, new Time[]{val12});
                break;
            case java.sql.Types.TIMESTAMP:
                java.sql.Timestamp val13 = rs.getTimestamp(name);
                method = o.getClass().getMethod(getSetMethodName("set", buffer, name), new Class[]{java.sql.Timestamp.class});
                method.invoke(o, new Timestamp[]{val13});
                break;
        }
    }

    public void addProperty(StringBuilder buffer, int type, String name,
            BeanGenerator bean) throws Exception {
        Class clsType = null;
        switch (type) {
            case java.sql.Types.CHAR:
            case java.sql.Types.VARCHAR:
            case java.sql.Types.LONGVARCHAR:
                clsType = String.class;
                break;
            case java.sql.Types.NUMERIC:
            case java.sql.Types.DECIMAL:
                clsType = java.math.BigDecimal.class;
                break;
            case java.sql.Types.BIT:
                clsType = java.lang.Boolean.class;
                break;
            case java.sql.Types.TINYINT:
                clsType = java.lang.Byte.class;
                break;
            case java.sql.Types.SMALLINT:
                clsType = java.lang.Short.class;
                break;
            case java.sql.Types.INTEGER:
                clsType = java.lang.Integer.class;
                break;
            case java.sql.Types.BIGINT:
                clsType = java.lang.Long.class;
                break;
            case java.sql.Types.REAL:
                clsType = java.lang.Float.class;
                break;
            case java.sql.Types.FLOAT:
            case java.sql.Types.DOUBLE:
                clsType = java.lang.Double.class;
                break;
            case java.sql.Types.BINARY:
            case java.sql.Types.VARBINARY:
            case java.sql.Types.LONGVARBINARY:
                clsType = java.lang.Byte[].class;
                break;
            case java.sql.Types.DATE:
                clsType = java.util.Date.class;
                break;
            case java.sql.Types.TIME:
                clsType = java.sql.Time.class;
                break;
            case java.sql.Types.TIMESTAMP:
                clsType = java.sql.Timestamp.class;
                break;
        }
        bean.addProperty(getSetPropertyName(buffer, name), clsType);
    }

    public int getCount(DataSource ds, String sql) throws Exception {
        int cnt = 0;
        int p = sql.indexOf("select ");
        int e = sql.indexOf(" from");
        String count = "select count(*) as cnt" + sql.substring(e, sql.length());
        log.info("SQL=" + count);
        Connection conn = ds.getConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(count.toLowerCase());
        if (rs.next()) {
            cnt = rs.getInt(1);
        }
        rs.close();
        stmt.close();
        conn.close();
        return cnt;
    }

    public List parseObject(DataSource ds, String preSql, String uid, int offset, int endRecord) throws Exception {
        String sql = preSql.toLowerCase();
        int p = sql.indexOf("select ");
        int e = sql.indexOf(" from");
        StringBuilder s = new StringBuilder();
        s.append("WITH TBL_RESULTS AS ( ");
        s.append(sql.substring(0, e)).append(", ROW_NUMBER() OVER(ORDER BY ");
        s.append(uid).append(") AS RowNum ");
        s.append(sql.substring(e, sql.length()));
        s.append(") SELECT * FROM TBL_RESULTS WHERE RowNum >= ").append(offset);
        s.append(" AND RowNum <= ").append(endRecord);
        List ls = new ArrayList();
        StringBuilder buffer = new StringBuilder();
        Connection conn = ds.getConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(s.toString());
        ResultSetMetaData rsMeta = rs.getMetaData();
        while (rs.next()) {
            BeanGenerator bean = new BeanGenerator();
            for (int i = 1; i <= rsMeta.getColumnCount(); i++) {
                String name = rsMeta.getColumnName(i);
                int type = rsMeta.getColumnType(i);
                addProperty(buffer, type, name, bean);
            }
            Object o = bean.create();
            for (int i = 1; i <= rsMeta.getColumnCount(); i++) {
                String name = rsMeta.getColumnName(i);
                int type = rsMeta.getColumnType(i);
                this.invoke(buffer, type, name, null, o, rs);
            }
            ls.add(o);
        }
        rs.close();
        stmt.close();
        conn.close();
        return ls;
    }
}
