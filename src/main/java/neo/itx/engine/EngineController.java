/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.itx.engine;

import java.io.File;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletContext;
import neo.itx.engine.service.DBService;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EngineController {

    @Autowired
    private Environment env;

    @Autowired
    private ServletContext servletContext;

    @Autowired
    private DBService dbService;

    @Autowired
    private ElasticsearchOperations elasticsearchOperations;

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(value = "/execute",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseEngine> execute(@RequestBody Map<String, Object> payload) {
        ResponseEngine res = new ResponseEngine();
        res.setCode(200);
        res.setMessage("SUCCESS");
        String sessionId = UUID.randomUUID().toString();
        try {
            log.info("Transaction Start..,");
            String name = (String) payload.get("sessionName");
            String indexCoordinate = (String) payload.get("indexCoordinate");
            int threadSize = (payload.get("threadSize") == null)
                    ? Integer.parseInt(env.getProperty("thread.size"))
                    : Integer.parseInt((String) payload.get("threadSize"));
            int limit = (payload.get("limit") == null)
                    ? Integer.parseInt(env.getProperty("record.limit"))
                    : Integer.parseInt((String) payload.get("limit"));
            File fileSession = new File(servletContext.getRealPath("/WEB-INF/scripts/" + name + ".xml"));
            Hashtable params = new Hashtable();
            Iterator its = payload.keySet().iterator();
            while (its.hasNext()) {
                String key = (String) its.next();
                if (!key.equals("sessionName")
                        || !key.equals("indexCoordinate") || !key.equals("threadSize")) {
                    params.put(key, payload.get(key));
                }
            }

            ParserXML parserXml = new ParserXML();
            SynchronizeQuery sync = parserXml.parser(fileSession, params);
            int totalRecord = dbService.findCount(params, sync.getQuery().toLowerCase());
            dbService.saveTransactionBegin(sessionId, name, indexCoordinate, totalRecord, 0);
            boolean c = false;
            try {
                c = elasticsearchOperations.indexExists(indexCoordinate.toLowerCase());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            if (c == false) {
                elasticsearchOperations.createIndex(indexCoordinate.toLowerCase());
            }
            (new Thread(new Asynchronize(this.env.getProperty("host.connect"), sessionId, dbService, elasticsearchOperations,
                    indexCoordinate, sync, params, limit, threadSize))).start();
            log.info("Transaction Success..,");
            res.setData(sessionId);
        } catch (Exception ex) {
            res.setCode(500);
            res.setMessage(String.format("Transaction error:%s", ex.getMessage()));
            res.setData("Recevice file fail...");
            log.error("Error:", ex);
            ex.printStackTrace();
        }
        return new ResponseEntity<ResponseEngine>(res, HttpStatus.OK);
    }

    @RequestMapping(value = "/checkstatus",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> checkStatus(@RequestBody Map<String, Object> payload) {
        JSONObject json = new JSONObject();
        json.put("code", 200);
        json.put("message", "success");
        String sessionId = (String) payload.get("sessionId");
        try {
            json.put("data", dbService.checkStatus(sessionId));
        } catch (Exception ex) {
            json.put("code", 500);
            json.put("message", "Error:" + ex.getMessage());
            ex.printStackTrace();
        }
        return new ResponseEntity<String>(json.toString(), HttpStatus.OK);
    }
}
