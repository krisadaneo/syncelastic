/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.itx.engine;

import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author krisada
 */
public class Pagination {

    private Pagination() {
    }

    public static List<Paging> find(int totalRecord, int limit) {
        List<Paging> ps = new ArrayList<Paging>();
        int totalPage = (totalRecord / limit);
        int mod = (totalRecord % limit);
        if (mod > 1) {
            totalPage = totalPage + 1;
        }
        int selectPage = 1;
        for (selectPage = 1; selectPage <= totalPage; selectPage++) {
            int startRec = (selectPage == 1) ? 1 : (((selectPage - 1) * limit) + 1);
            int endRec = (selectPage == totalPage) ? totalRecord : (selectPage * limit);
            Paging paging = new Paging((selectPage + 1), startRec, endRec, totalRecord);
            ps.add(paging);
            System.out.println("TotalPage:" + totalPage + " mod:" + mod + " startRec:" + startRec + " endRec:" + endRec);
        }
        return ps;
    }
/*
    public static void main(String args[]) {
        int totalRecord = 10024;
        int limit = 1000;
        Pagination.find(totalRecord, limit);
    }
*/
}
