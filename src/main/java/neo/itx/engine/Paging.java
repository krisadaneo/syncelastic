/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.itx.engine;

/**
 *
 * @author krisada
 */
public class Paging {
    private int pageNo;
    private int startRecord;
    private int endRecord;
    private int totalRecord;

    public Paging(int pageNo, int startRecord, int endRecord, int totalRecord) {
        this.pageNo = pageNo;
        this.startRecord = startRecord;
        this.endRecord = endRecord;
        this.totalRecord = totalRecord;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getStartRecord() {
        return startRecord;
    }

    public void setStartRecord(int startRecord) {
        this.startRecord = startRecord;
    }

    public int getEndRecord() {
        return endRecord;
    }

    public void setEndRecord(int endRecord) {
        this.endRecord = endRecord;
    }

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }
    
    
}
