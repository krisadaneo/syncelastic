/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.itx.engine;

import java.util.Hashtable;

/**
 *
 * @author krisada
 */
public class SynchronizeQuery {
    private String classMap;
    private Hashtable params = null;
    private String query = null;
    
    public SynchronizeQuery() {
        params = new Hashtable();
    }
    
    public Hashtable getParams() {
        return params;
    }
    
    public void pushParam(Param p) {
        params.put(p.getName(), p.getValue());
    }
    
    public Object getParam(String name) {
        return params.get(name);
    }
    
    public void setQuery(String q) {
        this.query = q;
    }
    
    public String getQuery() {
        return query;
    }

    public String getClassMap() {
        return classMap;
    }

    public void setClassMap(String classMap) {
        this.classMap = classMap;
    }
    
}
