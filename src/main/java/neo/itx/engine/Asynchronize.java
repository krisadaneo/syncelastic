/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.itx.engine;

import java.util.Hashtable;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import neo.itx.engine.service.DBService;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;

/**
 *
 * @author krisada
 */
public class Asynchronize implements Runnable {

    private String sessionId;
    private DBService dbService;
    private ElasticsearchOperations elasticsearchOperations;
    private String indexCoordinate;
    private SynchronizeQuery sync;
    private Hashtable params;
    private int limit = 0;
    private int threadSize = 0;
    private String host;

    public Asynchronize(String host, String sessionId, DBService dbService, ElasticsearchOperations elasticsearchOperations,
            String indexCoordinate, SynchronizeQuery sync, Hashtable params, int limit, int threadSize) {
        this.host = host;
        this.sessionId = sessionId;
        this.dbService = dbService;
        this.elasticsearchOperations = elasticsearchOperations;
        this.indexCoordinate = indexCoordinate;
        this.sync = sync;
        this.params = params;
        this.limit = limit;
        this.threadSize = threadSize;
    }

    @Override
    public void run() {
        try {
            int totalRecord = dbService.findCount(params, sync.getQuery().toLowerCase());
            List<Paging> paging = Pagination.find(totalRecord, limit);
            ExecutorService executor = Executors.newFixedThreadPool(threadSize);
            for (int i = 0; i < paging.size(); i++) {
                Paging p = paging.get(i);
                executor.execute(new EngineTask(this.host, sessionId, p.getPageNo(), dbService, elasticsearchOperations,
                        indexCoordinate, sync, params, p.getStartRecord(), p.getEndRecord()));
            }
            executor.shutdown();
            while (!executor.isTerminated()) {
            }
            dbService.saveTransactionEnd(sessionId);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
