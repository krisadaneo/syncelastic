/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.itx.engine;

import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author krisada
 */
public class ParserXML {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    private Param getParam(Node node, Hashtable mparams) {
        String name = node.getAttributes().getNamedItem("name").getNodeValue();
        Object value = "";
        if (mparams.containsKey(name)) {
            value = mparams.get(name);
        } else {
            if (node.getAttributes().getNamedItem("value") != null) {
                value = node.getAttributes().getNamedItem("value").getNodeValue();
            }
        }
        return new Param(name, value);
    }

    public SynchronizeQuery parser(File file, Hashtable mparams) {
        SynchronizeQuery sync = new SynchronizeQuery();
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            doc.getDocumentElement().normalize();
            log.info("Root element :" + doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName("param");
            log.info("----------------------------");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                log.info("Current Element :" + nNode.getNodeName());
                Param param = this.getParam(nNode, mparams);
                log.info("Param: %s", param.toString());
                sync.pushParam(param);
            }
            NodeList nodeQuery = doc.getElementsByTagName("query");
            String text = nodeQuery.item(0).getTextContent();
            Hashtable params = sync.getParams();
            String tmp = "";
            for (Enumeration enums = params.keys(); enums.hasMoreElements();) {
                String key = (String) enums.nextElement();
                Object val = sync.getParam(key);
                if (val.getClass().getName().equals("java.lang.String")) {
                    tmp = text.replaceAll(String.format("\\$\\{%s\\}", key), "'" + (String) val + "'");
                } else {
                    if (val.getClass().getName().equals("java.lang.Boolean")) {
                        tmp = text.replaceAll(String.format("\\$\\{%s\\}", key), ((Boolean) val).toString());
                    } else {
                        if (val.getClass().getName().equals("java.lang.Integer")) {
                            tmp = text.replaceAll(String.format("\\$\\{%s\\}", key), ((Integer) val).toString());
                        } else {
                            if (val.getClass().getName().equals("java.lang.Double")) {
                                tmp = text.replaceAll(String.format("\\$\\{%s\\}", key), ((Double) val).toString());
                            } else {
                                if (val.getClass().getName().equals("java.lang.Long")) {
                                    tmp = text.replaceAll(String.format("\\$\\{%d\\}", key), ((Long) val).toString());
                                } else {
                                    tmp = text.replaceAll(String.format("\\$\\{%s\\}", key), "'" + (String) val + "'");
                                }
                            }
                        }
                    }
                }
                text = tmp;
            }
            log.info("Query:" + tmp);
            sync.setQuery(text);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sync;
    }

    

    


    /*
    public List parseObject(String sql) throws Exception {
        List ls = new ArrayList();
        StringBuilder buffer = new StringBuilder();
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        String connectionUrl = "jdbc:sqlserver://localhost\\cfrm_exim_ic:1433;databaseName=cfrm_exim_ic";
        Connection conn = DriverManager.getConnection(connectionUrl, "sa", "P@ss4321");
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);
        ResultSetMetaData rsMeta = rs.getMetaData();
        while (rs.next()) {
            BeanGenerator bean = new BeanGenerator();
            for (int i = 1; i <= rsMeta.getColumnCount(); i++) {
                String name = rsMeta.getColumnName(i);
                int type = rsMeta.getColumnType(i);
                addProperty(buffer, type, name, bean);
            }
            Object o = bean.create();
            for (int i = 1; i <= rsMeta.getColumnCount(); i++) {
                String name = rsMeta.getColumnName(i);
                int type = rsMeta.getColumnType(i);
                this.invoke(buffer, type, name, null, o, rs);
            }
            ls.add(o);
        }
        rs.close();
        stmt.close();
        conn.close();
        return ls;
    }*/

    /*
    public static void main(String args[]) throws Exception {
        ParserXML p = new ParserXML();
        Hashtable params = new Hashtable();
        params.put("id", 0);
        SynchronizeQuery query = p.parser(new File("/home/krisada/NetBeansProjects/ParserDOM/src/main/resources/example.xml"), params);
        System.out.println("SQL=" + query.getQuery());
        List ls = p.parseObject(query.getQuery());
        for (int i = 0; i < ls.size(); i++) {
            System.out.println(ls.get(i));
        }
    }*/
}
