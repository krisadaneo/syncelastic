/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.itx.engine;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author krisada
 */
public class ElasticSearchClient {

    private Logger log = LoggerFactory.getLogger(this.getClass());
    
    private ElasticSearchClient() {
        
    }
    
    public static ElasticSearchClient client() {
        return new ElasticSearchClient();
    } 

    public JSONObject search(String host, String index, String prop, String uid) {
        String query = String.format("{\"query\":{\"match\":{\"$cglib_prop_%s\":{\"query\":\"%s\"}}}}", prop, uid);
        log.info(query);
        try {
            String jsonString = this.request(host, index, query);
            log.info("return:"+ jsonString);
            JSONObject json = new JSONObject(jsonString);
            JSONArray objs = json.getJSONObject("hits").getJSONArray("hits");
            return objs.getJSONObject(0);
        } catch(JSONException ex) {
            log.error("Search Error", ex);
        }
        return null;
    }
    
    private String request(String host, String index, String jsonInputString) {
        try {
            URL url = new URL(String.format("http://%s:9200/%s/_search", host, index));

            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");

            con.setRequestProperty("Content-Type", "application/x-ndjson; utf-8");
            con.setRequestProperty("Accept", "application/json");

            con.setDoOutput(true);

            try ( OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }

            int code = con.getResponseCode();
            log.info(String.format("Code : %d", code));

            try ( BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                return response.toString();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
    
    /*
    public static void main(String args[]) {
        JSONObject json = ElasticSearchClient.client().search(
                "127.0.0.1", "imp_sanc_idnumber_idx", "uniqueKey", 
                "3cea6ed252230bf22529ed13d74c04557351a5d99734c958784e5f2ee59492ca");
        System.out.println(json.toString());
    }*/
}
