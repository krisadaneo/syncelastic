/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.itx.engine.dao;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author krisada
 */

public class ImpSancName implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String entityId;
    private Date lastModified;
    private String lastModifiedBy;
    private String sanctionGroup;
    private Date created;
    private String createdBy;
    private String systemId;
    private String nameType;
    private String firstName;
    private String middleName;
    private String surname;
    private String suffix;
    private String entityName;
    private String englishName;
    private String singleStringName;
    private String originalScriptName;
    private String falsePositive;
    private String falsePositiveExpiryDt;
    private String approvalStatus;
    private String subEntityId;
    private String category;
    private String highRiskCountry;
    private String highRiskCountryScore;
    private String infoSource;
    private String orgId;
    private String action;
    private String idRefNo;
    private String batchNo;
    private Date batchDate;
    private String status;

    public ImpSancName() {
    }

    public ImpSancName(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getSanctionGroup() {
        return sanctionGroup;
    }

    public void setSanctionGroup(String sanctionGroup) {
        this.sanctionGroup = sanctionGroup;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getSystemId() {
        return systemId;
    }

    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }

    public String getNameType() {
        return nameType;
    }

    public void setNameType(String nameType) {
        this.nameType = nameType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public String getEnglishName() {
        return englishName;
    }

    public void setEnglishName(String englishName) {
        this.englishName = englishName;
    }

    public String getSingleStringName() {
        return singleStringName;
    }

    public void setSingleStringName(String singleStringName) {
        this.singleStringName = singleStringName;
    }

    public String getOriginalScriptName() {
        return originalScriptName;
    }

    public void setOriginalScriptName(String originalScriptName) {
        this.originalScriptName = originalScriptName;
    }

    public String getFalsePositive() {
        return falsePositive;
    }

    public void setFalsePositive(String falsePositive) {
        this.falsePositive = falsePositive;
    }

    public String getFalsePositiveExpiryDt() {
        return falsePositiveExpiryDt;
    }

    public void setFalsePositiveExpiryDt(String falsePositiveExpiryDt) {
        this.falsePositiveExpiryDt = falsePositiveExpiryDt;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getSubEntityId() {
        return subEntityId;
    }

    public void setSubEntityId(String subEntityId) {
        this.subEntityId = subEntityId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getHighRiskCountry() {
        return highRiskCountry;
    }

    public void setHighRiskCountry(String highRiskCountry) {
        this.highRiskCountry = highRiskCountry;
    }

    public String getHighRiskCountryScore() {
        return highRiskCountryScore;
    }

    public void setHighRiskCountryScore(String highRiskCountryScore) {
        this.highRiskCountryScore = highRiskCountryScore;
    }

    public String getInfoSource() {
        return infoSource;
    }

    public void setInfoSource(String infoSource) {
        this.infoSource = infoSource;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getIdRefNo() {
        return idRefNo;
    }

    public void setIdRefNo(String idRefNo) {
        this.idRefNo = idRefNo;
    }

    public String getBatchNo() {
        return batchNo;
    }

    public void setBatchNo(String batchNo) {
        this.batchNo = batchNo;
    }

    public Date getBatchDate() {
        return batchDate;
    }

    public void setBatchDate(Date batchDate) {
        this.batchDate = batchDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ImpSancName)) {
            return false;
        }
        ImpSancName other = (ImpSancName) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "neo.itx.engine.dao.ImpSancName[ id=" + id + " ]";
    }
    
}
