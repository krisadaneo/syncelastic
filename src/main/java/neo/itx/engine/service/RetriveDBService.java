/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.itx.engine.service;

import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import neo.itx.engine.repository.DBRepository;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author krisada
 */
@Service
public class RetriveDBService implements DBService {
    
    @Autowired
    private DBRepository dbRepository;

    public RetriveDBService() {
    }

    @Override
    public int findCount(Hashtable params, String sql) throws Exception {
        return dbRepository.findCount(params, sql);
    }
    
    @Override
    public List find(Hashtable params, String sql, String uid, int offset, int end) throws Exception {
        return dbRepository.find(params, sql, uid, offset, end);
    }

    @Override
    public boolean saveTransactionBegin(String sessionId, String sessionName, String indexco, int recordTotal, int recordStatus) throws Exception {
        return dbRepository.saveBegin(sessionId, sessionName, indexco, recordTotal, recordStatus);
    }

    @Override
    public boolean saveTransactionEnd(String sessionId) throws Exception {
        return dbRepository.saveEnd(sessionId);
    }

    @Override
    public int findTransactionStatus(String sessionId) throws Exception {
        return dbRepository.queryStatus(sessionId);
    }

    @Override
    public boolean saveTransactionStatus(String sessionId, int recordStatus) throws Exception {
        return dbRepository.saveStatus(sessionId, recordStatus);
    }

    @Override
    public JSONObject checkStatus(String sessionId) throws Exception {
        return dbRepository.checkStatus(sessionId);
    }
    
    
    
}
