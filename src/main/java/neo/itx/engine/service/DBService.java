/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.itx.engine.service;

import java.util.Hashtable;
import java.util.List;
import org.json.JSONObject;

/**
 *
 * @author krisada
 */
public interface DBService {

    public int findCount(Hashtable params, String sql) throws Exception;

    public List find(Hashtable params, String sql, String uid, int offset, int limit) throws Exception;

    public boolean saveTransactionBegin(String sessionId, String sessionName, String indexco, int recordTotal, int recordStatus) throws Exception;

    public boolean saveTransactionStatus(String sessionId, int recordStatus) throws Exception;

    public boolean saveTransactionEnd(String sessionId) throws Exception;

    public int findTransactionStatus(String sessionId) throws Exception;
    
    public JSONObject checkStatus(String sessionId) throws Exception;
}
