/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.itx.engine.repository;

import java.util.Hashtable;
import java.util.List;
import org.json.JSONObject;

/**
 *
 * @author krisada
 */
public interface DBRepository {

    public List find(Hashtable params, String sql, String uid, int offset, int limit) throws Exception;
    public int findCount(Hashtable params, String sql) throws Exception;
    public boolean saveBegin(String sessionId, String sessionName, String indexco, int recordTotal, int recordStatus) throws Exception;
    public boolean saveStatus(String sessionId, int recordStatus) throws Exception;
    public boolean saveEnd(String sessionId) throws Exception;
    public int queryStatus(String sessionId) throws Exception;
    public JSONObject checkStatus(String sessionId) throws Exception; 
}