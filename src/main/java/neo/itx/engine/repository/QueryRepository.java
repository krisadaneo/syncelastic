/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package neo.itx.engine.repository;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import javax.sql.DataSource;
import neo.itx.engine.Transformer;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 *
 * @author krisada
 */
@Repository
public class QueryRepository implements DBRepository {

    @Autowired
    private DataSource dataSource;

    private Transformer transformer;

    public QueryRepository() {
        transformer = new Transformer();
    }

    @Override
    public List find(Hashtable params, String sql, String uid, int offset, int end) throws Exception {
        List ls = null;
        ls = transformer.parseObject(dataSource, sql, uid, offset, end);
        if (ls == null) {
            ls = new ArrayList();
        }
        return ls;
    }

    @Override
    public int findCount(Hashtable params, String sql) throws Exception {
        return transformer.getCount(dataSource, sql);
    }

    @Override
    public int queryStatus(String sessionId) throws Exception {
        int cnt = 0;
        Connection conn = this.dataSource.getConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(String.format("SELECT record_status FROM tbl_transaction WHERE session_id = '%s'", sessionId));
        if (rs.next()) {
            cnt = rs.getInt(1);
        }
        rs.close();
        stmt.close();
        conn.close();
        return cnt;
    }

    @Override
    public boolean saveBegin(String sessionId, String sessionName, String indexco, int recordTotal, int recordStatus) throws Exception {
        Connection conn = this.dataSource.getConnection();
        Statement stmt = conn.createStatement();
        stmt.executeUpdate(
                String.format("INSERT INTO tbl_transaction(session_id, session_name, indexco, record_total, record_status, start_time, end_time) VALUES('%s', '%s', '%s', %d, 0, CURRENT_TIMESTAMP, NULL)", sessionId, sessionName, indexco, recordTotal));
        stmt.close();
        conn.close();
        return true;
    }

    @Override
    public boolean saveEnd(String sessionId) throws Exception {
        Connection conn = this.dataSource.getConnection();
        Statement stmt = conn.createStatement();
        stmt.executeUpdate(
                String.format("UPDATE tbl_transaction SET end_time = CURRENT_TIMESTAMP WHERE session_id = '%s'", sessionId));
        stmt.close();
        conn.close();
        return true;
    }

    @Override
    public boolean saveStatus(String sessionId, int recordStatus) throws Exception {
        Connection conn = this.dataSource.getConnection();
        Statement stmt = conn.createStatement();
        stmt.executeUpdate(
                String.format("UPDATE tbl_transaction SET record_status = %d WHERE session_id = '%s'", recordStatus, sessionId));
        stmt.close();
        conn.close();
        return true;
    }

    @Override
    public JSONObject checkStatus(String sessionId) throws Exception {
        JSONObject o = new JSONObject();
        Connection conn = this.dataSource.getConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(String.format("SELECT session_id, session_name, indexco, record_total, record_status, start_time, end_time FROM tbl_transaction WHERE session_id = '%s'", sessionId));
        if( rs.next() ) {
            o.put("sessionId", rs.getString("session_id"));
            o.put("sessionName", rs.getString("session_name"));
            o.put("indexCoodinate", rs.getString("indexco"));
            o.put("recordTotal", rs.getInt("record_total"));
            o.put("recordStatus", rs.getInt("record_status"));
            o.put("startTime", new java.util.Date(rs.getDate("start_time").getTime()));
            o.put("endTime", new java.util.Date(rs.getDate("end_time").getTime()));
        }
        rs.close();
        stmt.close();
        conn.close();
        return o;
    }
    
    

}
